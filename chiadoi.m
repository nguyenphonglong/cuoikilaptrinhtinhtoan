function [x,k]= chiadoi(f,a,b,ep,Nmax)
    % ??u v�o [a,b] l� kho?ng ph�n li nghi?m, ep l� sai s?, Nmax l� s? v�ng
    % l?p t?i ?a
    % k?t qu? tr? ra x l� nghi?m c?a ph??ng tr�nh, k l� s? v�ng l?p
    syms x;
    k=0;
    while(k<Nmax && abs(a-b)>ep)
        k=k+1;
        x=(a+b)/2;
        if subs(f,x)==0
            return 
        else 
            if subs(f,x)*subs(f,a)<0
               b=(a+b)/2;
            else 
                a=(a+b)/2;
            end
            
        end
    end
end