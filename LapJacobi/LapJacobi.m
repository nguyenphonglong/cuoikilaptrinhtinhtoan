function [x,k]=LapJacobi(A,b,x0,ep,Nmax)
    n=length(b);
    if(~checkCheoTroi(A))
        error('Ma tran A khong cheo troi')
    end
    % Tao ma tran B
    B=zeros(n,n);
    for i=1:n
        B(i,:)=-A(i,:)/A(i,i);
        B(i,i)=0;
    end
    % Tao vecto cot g
    g=zeros(n,1);
    for i=1:n
        g(i)=b(i)/A(i,i);
    end
    normB=max(sum(abs(B),2)); % chuan vo cuc
    k=1;
    x=B*x0+g;
    while(k<Nmax && ep < (normB/(1-normB)*max(abs(x-x0))))  % dieu kien lap
         k=k+1;
         x0=x;
         x=B*x0+g;
    end
end