function [p] = langrange(X, Y)
% ??u v�o X,Y l� m?ng c�c th�ng s?
% ??u ra p l� ?a th?c theo x
    n = length(X);
    if (length(Y)~=n)
        error('X va Y phai cung so phan tu. Moi nhap lai: ');
    end
    syms x;
    s = 0;
    for i= 1:n
        bienPhu = Y(i);
        for j=1:n
            if i~=j
                bienPhu=bienPhu*(x-X(j))/(X(i)-X(j));
            end
        end
        s=s+bienPhu;
    end
    p=s;
    p=collect(p);
end
