function [x_rk4,y_rk4]=Rk4(f,x0,xNgang,y0,n)
    % n l� so doan chia
    syms x y;
    x0_bandau=x0;
    h=(xNgang-x0)/n;
    x_rk4=linspace(x0,xNgang,n+1);
    y_rk4=zeros(1,n+1);
    y_rk4(1)=y0;
    for chiso=2:n+1
        k1=h*subs(subs(f,x,x0),y,y0);
        k2=h*subs(subs(f,x,x0+h/2),y,y0+k1/2);
        k3=h*subs(subs(f,x,x0+h/2),y,y0+k2/2);
        k4=h*subs(subs(f,x,x0+h),y,y0+k3);
        y1=y0+1/6*(k1+2*k2+2*k3+k4);
        y_rk4(chiso)=y1;
        y0=y1;
        x0=x0+h;
    end
    hold on;
    x_dung=linspace(x0_bandau,xNgang,100);
    dodai=length(x_dung);
    for i =1:dodai
        y_dung(i)=2*exp(x_dung(i))-x_dung(i)-1;
    end
    plot(x_dung,y_dung);
    plot(x_rk4,y_rk4);
end