function isCheoTroi = checkCheoTroi(A)
    isCheoTroi = all(2*abs(diag(A))>sum(abs(A),2));
end