function [x,k]= daycung(f,x0,a,b,ep,Nmax)
    if subs(f,x0)*subs(diff(f,2),(a+b)/2)>=0
        error('Chon x0 khong thoa man');
    end
    if subs(f,a)*subs(f,x0)<0
        d=a;
    else
        d=b;
    end
    bienPhu1=abs(subs(diff(f),a));
    bienPhu2=abs(subs(diff(f),b));
    M=max(bienPhu1,bienPhu2);
    m=min(bienPhu1,bienPhu2);
    k=1;
    x=x0-((d-x0)/(subs(f,d)-subs(f,x0)))*subs(f,x0);
    while(k<Nmax && (M-m)/m*abs(x-x0)>ep)
        k=k+1;
        x0=x;
        x=x0-((d-x0)/(subs(f,d)-subs(f,x0)))*subs(f,x0);
    end
    x=double(x);
end