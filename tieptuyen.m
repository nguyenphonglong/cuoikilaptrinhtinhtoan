function [x,k]= tieptuyen(f,a,b,ep,Nmax)
    if(subs(f,a)*subs(diff(f,2),a)>0)
        x=a;
    else
        x=b;
    end
    if(subs(f,a)*subs(f,b)>=0)
        error(['[',num2str(a),',',num2str(b),'] khong phai la khoang phan li nghiem. Moi nhap lai']);
    else
        k=0;
        bienPhu1=abs(subs(diff(f),a));
        bienPhu2=abs(subs(diff(f),b));
        m=min(bienPhu1,bienPhu2);
        while(k<Nmax && abs(subs(f,x))/m >ep)
            k=k+1;
            x=x-subs(f,x)/subs(diff(f),x);          
        end
    end
    x=double(x);
end